package objectProgramming;

import model.Directory;
import model.CrawlerFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Crawler {
    public static void run() {
        // Using Scanner to get input from user
        Scanner input = new Scanner(System.in);

        // User input
        int userInput = -1;

        // Menu
        System.out.println("Given a path, this program will read the files structure and print it to the console. \n" +
                "0 - exit \n" +
                "1 - provide a path \n");
        System.out.println("Please choose one of the options above. \n");

        while (userInput != 0) {
            try {
                userInput = input.nextInt();

                switch (userInput) {
                    case 0:
                        System.out.println("Bye");
                        break;
                    case 1:
                        handlePath();
                        break;
                    default:
                        System.out.println("Option " + userInput + " is not available. " + "Please do one of the following: \n" +
                                "- enter 0 to exit, \n" +
                                "- enter 1 to provide a path.");
                }
            } catch (Exception exception){
                System.out.println("Invalid input. Please provide a number:");
                input.next();
            }
        }
    }

    public static void printOptions() {
        System.out.println("Want to try again? \n" +
                "0 - exit \n" +
                "1 - provide a path \n");
        System.out.println("Please choose one of the options above.");
    }

    public static void handlePath() {
        // Get path from user
        Scanner input = new Scanner(System.in);
        System.out.println("Please provide a path:");
        String pathInput = input.nextLine();
        try {
            // Check if path is valid
            File path = new File(pathInput);
            if (path.exists()){
                CrawlerFile structure = generateStructure(path, 0);
                System.out.println(structure.toString());
                // Print options for the user after printing out the folder structure
                printOptions();
            } else {
                System.out.println("Invalid path.\nIt's maybe just a typo. Make sure this path exists in your operating system.");
            }
        } catch (Exception exception) {
            System.out.println("Invalid input. Input should not be a number.");
        }
    }

    public static Date getDate(File path)  {
        Date date = new Date();
        try {
            // attrs.creationTime() is not a Date() and has to be parsed
            BasicFileAttributes attrs = Files.readAttributes(path.toPath(), BasicFileAttributes.class);
            FileTime time = attrs.creationTime();
            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
            String formattedDate = dateFormatter.format(time.toMillis());
            date = dateFormatter.parse(formattedDate);
            return date;
        } catch(IOException exception) {
            System.out.println("Unable to read file attributes.");
        }
        catch(ParseException exception) {
            System.out.println("Unable to parse date.");
        }
        return date;
    }

    public static List<CrawlerFile> convertToMyFileList(File[] javaFiles, Integer depth) {
        // Get file list of a Directory
        List<CrawlerFile> crawlerFileList = new ArrayList<>();
        if (javaFiles == null) {
            return crawlerFileList;
        }
        for (File file : javaFiles) {
            // Recursion: function 'generateStructure' checks files and executes the function on them again
            crawlerFileList.add(generateStructure(file, depth + 1));
        }
        return crawlerFileList;
    }

    public static CrawlerFile generateStructure(File path, Integer depth) {
        // If path points to a file - return MyFile object
        // else - return Directory
        if (!path.isDirectory()) {
            // Get creation date
            Date creationDate = getDate(path);
            return new CrawlerFile(path.getName(), creationDate, depth);
        } else {
            // Get creation date
            Date creationDate = getDate(path);
            // Get file list
            List<CrawlerFile> listOfCrawlerFile = convertToMyFileList(path.listFiles(), depth);
            return new Directory(path.getName(), creationDate, listOfCrawlerFile, depth);
        }
    }

}
