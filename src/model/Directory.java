package model;

import java.util.Date;
import java.util.List;

public class Directory extends CrawlerFile {
        private List<CrawlerFile> fileList;

        public Directory(String name, Date creationDate, List<CrawlerFile> fileList, Integer depth) {
                super(name, creationDate, depth);
                this.fileList = fileList;
        }

        @Override
        public boolean isDirectory() {
                return true;
        }

        @Override
        public String toString() {
                StringBuilder indentation = new StringBuilder();;
                indentation.append("\t".repeat(Math.max(0, depth - 1)));
                String ret = indentation + name + " " + creationDate + "\n";
                for(CrawlerFile file : fileList) {
                        ret = ret + file.toString();
                }
                return ret;
        }
}

