package model;

import java.util.Date;

public class CrawlerFile {
    protected final Integer depth;
    protected String name;
    protected Date creationDate;

    public CrawlerFile(String name, Date creationDate, Integer depth) {
        this.name = name;
        this.creationDate = creationDate;
        this.depth = depth;
    }

    public boolean isDirectory() {
        return false;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        StringBuilder indentation = new StringBuilder();;
        indentation.append("\t".repeat(Math.max(0, depth - 1)));
        return indentation + name + " " + creationDate + "\n";
    }
}
