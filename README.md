![Project icon](images/project-icon.png)

# Crawler

The content of the task:
1. Create a class 'Folder' with the following fields:
- Name
- Creation date
- File list
- Is folder

2. Create a class 'File' with the following fields:
- Name
- Creation date
- Is file

**File list - contains Folder and File objects**

Using above classes, write a crawler. This crawler should create a structure from the given path to the directory and display it as tree.

**Example:**
```
*Root folder
*-Folder1
    *-Folder2
        *-Plik1
    *-Plik2
```

## Changes
- Class File renamed to crawlerFile.
- Class Folder renamed to Directory.
- Added 'depth' field.

## Assessment criteria
- Compliance of the program with the requirements.
- Crawler should use recursion.
- Code compliance with good Java programming conventions and practices.

## Instructions
1. Clone this repository.
2. Run project in IDE.

## Program screenshot
![Crawler screenshot](images/crawler-screenshot.png)

## Helpful links
- https://www.baeldung.com/java-string-to-date 
- https://docs.oracle.com/javase/8/docs/api/java/sql/Timestamp.html#toLocalDateTime--
- https://www.javatpoint.com/java-string-to-date
- https://www.oracle.com/technical-resources/articles/java/java7exceptions.html
- https://stackoverflow.com/questions/12994342/how-to-read-whitespace-with-scanner-next
- https://icons8.com/illustrations
- https://www.makeareadme.com/
- https://guides.github.com/features/mastering-markdown/

## License
[MIT](https://choosealicense.com/licenses/mit/)
